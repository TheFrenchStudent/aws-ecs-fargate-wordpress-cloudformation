# Stack AWS ECS Fargate WordPress via CloudFormation

<p align="center">
  <img src="images/logo-projet.png" alt="Logo projet DevOps"/>
</p>

## Sommaire

[[_TOC_]]

## Introduction

Ce projet permet de lancer sur AWS un cluster ECS managé par Fargate et déployant un site WordPress loadbalancé avec une BDD RDS et un CDN.

Les prérequis sont les suivants :
* Accès à un compte AWS avec au minimum 100$ de crédits.
* Les rôles adaptés pour créer et manager les ressources réseaux, CloudFormation, ECS, EC2 (pour le loadbalancer et les security groups), RDS, CloudFront, EFS & Fargate nécessaires. Vous pouvez accorder à votre IAM utilisateur la politique AdministratorAccess pour plus de simplicité.
* Un accès AWS CLI via le terminal de votre choix. Il faut donc configurer votre IDE ou terminal bash pour qu'il soit connecté à votre compte AWS (voir [ce tutoriel](https://aws.amazon.com/fr/getting-started/guides/setup-environment/module-three/)).

Description des dossiers :
* ***challenge3*** : Contient le template CloudFormation pour déployer l'application et son infrastructure ainsi qu'un fichier bash qui déploie l'infrastructure complète avec le template CloudFormation.
* ***templates-cloudformation*** : Contient des templates CloudFormation pour s'aider à démarrer.
* ***images*** : Contient les images présentes dans ce README.

## Comment déployer l'infrastructure

Clonez le repo, puis rendez-vous dans le dossier *challenge3*. Exécutez ensuite le fichier bash ```deploy.sh``` (assurez-vous que le fichier soit exécutable au préalable). Ce fichier va déployer toute la stack et l'infrastrcuture nécessaire pour rendre notre site WordPress accessible via CloudFormation :

![Stack CloudFormation](images/wordpress-stack-cloudformation.png)

> __Attention__ :
Si vous obtenez une erreur 502 en vous rendant sur l'URL du site WordPress, cela signifie que la ou les tasks definition ne sont pas encore totalement up & running.

Vous pouvez regarder en détails dans le fichier ```deploy.sh``` ce qu'il fait. Des commentaires ont été ajoutés à cet effet.
On remarque à la fin du déploiement que notre service ECS managé par Fargate est créé, ainsi que les tasks definition, qui déterminent le nombre d'instances ECS suivant la charge et la définition de l'autoscaling :

![Task definition](images/wordpress-task-definition.png)

Le site est accessible, une fois le service ECS bien déployé :
![WordPress Login](images/wordpress-login.png)
![WordPress Home Page](images/wordpress-homepage.png)

## Test de charge

Avec l'utilitaire [hey](https://github.com/rakyll/hey?tab=readme-ov-file), on peut simuler une charge de trafic pour vérifier si notre site va tenir la charge. Pour un premier test, nous avons mis 10 000 requêtes sur notre URL :

```bash
[ec2-user@ip-172-31-33-243 ~]$ /hey_linux_amd64 -n 10000 "http://$(aws elbv2 describe-load-balancers \
  --names wof-load-balancer --region us-east-1 \
  --query 'LoadBalancers[].DNSName' --output text)/wp-admin/"
Summary:
  Total:        289.3132 secs
  Slowest:      7.6271 secs
  Fastest:      0.1706 secs
  Average:      1.4001 secs
  Requests/sec: 34.5646
  

Response time histogram:
  0.171 [1]     |
  0.916 [2974]  |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  1.662 [3680]  |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  2.408 [1706]  |■■■■■■■■■■■■■■■■■■■
  3.153 [1305]  |■■■■■■■■■■■■■■
  3.899 [293]   |■■■
  4.645 [19]    |
  5.390 [11]    |
  6.136 [4]     |
  6.881 [5]     |
  7.627 [2]     |


Latency distribution:
  10% in 0.2239 secs
  25% in 0.5004 secs
  50% in 1.3457 secs
  75% in 1.9679 secs
  90% in 2.7049 secs
  95% in 3.0023 secs
  99% in 3.4928 secs

Details (average, fastest, slowest):
  DNS+dialup:   0.0000 secs, 0.1706 secs, 7.6271 secs
  DNS-lookup:   0.0000 secs, 0.0000 secs, 0.0072 secs
  req write:    0.0000 secs, 0.0000 secs, 0.0037 secs
  resp wait:    0.7317 secs, 0.0850 secs, 6.8656 secs
  resp read:    0.0002 secs, 0.0001 secs, 0.0161 secs

Status code distribution:
  [200] 10000 responses
```
Puis avec 100 000 requêtes, on voit que le site répond en majorité en dessous de la seconde :

```bash
[ec2-user@ip-172-31-33-243 ~]$ /hey_linux_amd64 -n 100000 "http://$(aws elbv2 describe-load-balancers \
  --names wof-load-balancer --region us-east-1 \
  --query 'LoadBalancers[].DNSName' --output text)/wp-admin/"

Summary:
  Total:        1839.2877 secs
  Slowest:      6.8635 secs
  Fastest:      0.0673 secs
  Average:      0.9101 secs
  Requests/sec: 54.3689
  
  Total data:   17471 bytes
  Size/request: 0 bytes

Response time histogram:
  0.067 [1]     |
  0.747 [51229] |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  1.427 [24715] |■■■■■■■■■■■■■■■■■■■
  2.106 [15873] |■■■■■■■■■■■■
  2.786 [4686]  |■■■■
  3.465 [2820]  |■■
  4.145 [602]   |
  4.825 [57]    |
  5.504 [12]    |
  6.184 [3]     |
  6.864 [2]     |


Latency distribution:
  10% in 0.2012 secs
  25% in 0.2242 secs
  50% in 0.6751 secs
  75% in 1.4056 secs
  90% in 1.9466 secs
  95% in 2.5474 secs
  99% in 3.3272 secs

Details (average, fastest, slowest):
  DNS+dialup:   0.0000 secs, 0.0673 secs, 6.8635 secs
  DNS-lookup:   0.0000 secs, 0.0000 secs, 0.0075 secs
  req write:    0.0000 secs, 0.0000 secs, 0.0045 secs
  resp wait:    0.4797 secs, 0.0013 secs, 5.7239 secs
  resp read:    0.0002 secs, 0.0000 secs, 0.0198 secs

Status code distribution:
  [200] 99993 responses
  [500] 6 responses
  [502] 1 responses
```
Enfin, avec 500 000 requêtes, le site répond toujours en moyenne à moins d'une seconde en majorité du temps et des requêtes:

```bash
[ec2-user@ip-172-31-33-243 ~]$ /hey_linux_amd64 -n 500000 "http://$(aws elbv2 describe-load-balancers \
  --names wof-load-balancer --region us-east-1 \
  --query 'LoadBalancers[].DNSName' --output text)/wp-admin/"

Summary:
  Total:        4684.8987 secs
  Slowest:      5.3860 secs
  Fastest:      0.0013 secs
  Average:      0.4657 secs
  Requests/sec: 106.7259

  Total data:   122 bytes
  Size/request: 0 bytes

Response time histogram:
  0.001 [1]     |
  0.540 [380160]|■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  1.078 [82150] |■■■■■■■■■
  1.617 [25061] |■■■
  2.155 [8131]  |■
  2.694 [3852]  |
  3.232 [576]   |
  3.771 [30]    |
  4.309 [16]    |
  4.848 [16]    |
  5.386 [7]     |


Latency distribution:
  10% in 0.1848 secs
  25% in 0.2350 secs
  50% in 0.3275 secs
  75% in 0.5248 secs
  90% in 0.9385 secs
  95% in 1.2766 secs
  99% in 2.1101 secs

Details (average, fastest, slowest):
  DNS+dialup:   0.0000 secs, 0.0013 secs, 5.3860 secs
  DNS-lookup:   0.0000 secs, 0.0000 secs, 0.0058 secs
  req write:    0.0000 secs, 0.0000 secs, 0.0053 secs
  resp wait:    0.4650 secs, 0.0012 secs, 5.3853 secs
  resp read:    0.0007 secs, 0.0000 secs, 0.2302 secs

Status code distribution:
  [200] 499999 responses
  [502] 1 responses

[ec2-user@ip-172-31-33-243 ~]$
```

On voit bien que le load balancer reçoit la charge et tient la charge :

![Test de charge sur le LB](images/wordpress-test-charge.png)
![CPU Utilization](images/cpu-usage-target-ecs.png)

Et que nous avons 2 instances ECS en plus qui se créent au fur et à mesure, dû à la politique d'autoscaling, quand un pic de charge arrive (suite à la réalisation des tests de charge) :

![Target ALB ECS 3 containers](images/wordpress-containers-scaling-up-3-containers.png)
![Target ALB ECS](images/wordpress-containers-target-alb.png)

On remarque bien que 2 conteneurs sont dans une AZ, et les deux autres dans une autre AZ.

Ce changement est visible dans les événements du load balancer :

![Events LB](images/wordpress-scaling-up-events.png)

Puis, une fois le pic de charge passé, on remarque que deux conteneurs de chaque AZ sont décommissionnés, car la charge globale sur les services est redescendu en-dessous des 60% :

![Scaling down des instances ECS](images/wordpress-containers-target-alb-scaling-down.png)

## Configuration des instances

A noter qu'il est possible de modifier le sizing des instances ECS ainsi que l'autoscaling et la task definition associée, suivant les hits prévisionnels à chaque période de l'année. Pour ce qui est du sizing des containers, la brique de task definition suivante permet de presonnaliser les specs de vos conteneurs suivant vos besoins :

```json
"requiresCompatibilities": [
        "FARGATE"
    ],
    "cpu": "2048",
    "memory": "4096",
    "volumes": [
        {
            "name": "wordpress",
            "efsVolumeConfiguration": {
                "fileSystemId": "${WOF_EFS_FS_ID}",
                "transitEncryption": "ENABLED",
                "authorizationConfig": {
                    "accessPointId": "${WOF_EFS_AP}",
                    "iam": "DISABLED"
                }
            }
        }
```
Actuellement la capacité souhaitée de conteneurs est à 2, avec un maximum de 6. Suivant les hits et la charge observée, vous pouvez ajustez ces paramètres comme vous le souhaitez. **Verpex** recommande au moins 2Go de CPU pour un site avec beaucoup de trafic :
> __Recommandations__ :
*WordPress sites require a minimum of 512MB of RAM, but the amount required may vary based on factors such as website size, number of users, and plugins used. While a minimum of 1 GB of RAM is recommended for most sites, larger sites with heavy traffic or resource-intensive plugins may need 2 GB or more.*

## Contact

Pour toute question sur le projet, vous pouvez me contacter via *[LinkedIn](https://www.linkedin.com/in/hadrien-gobier-b16b5b14a/)*.